# methods: defaults, fetch, initialize


class Temperature

  def initialize(options)
    if options[:f]
      self.fahrenheit = options[:f]
    else
      self.celsius = options[:c]
    end
    # @f = options.fetch(:f, @c.fahrenheit)
    # @c = options.fetch(:c, @f.celsius)
  end

  def fahrenheit=(temp)
    @c = ftoc(temp)
  end

  def celsius=(temp)
    @c = temp
  end

  def ctof(temp)
    (temp * 9.0 / 5.0) + 32.0
  end

  def ftoc(temp)
    (temp - 32.0) * 5.0 / 9.0
  end

  def in_fahrenheit
    ctof(@c)
  end

  def in_celsius
    @c
  end

  def self.from_fahrenheit(temp)
    self.new(f: temp)
  end

  def self.from_celsius(temp)
    self.new(c: temp)
  end

end

class Celsius < Temperature
  def initialize(arg)
    @c = arg
    @f = ((@c * 9/5) + 32)
    @c
  end
end

class Fahrenheit < Temperature
  def initialize(arg)
    @f = arg
    @c = ((arg - 32) * 5/9)
    @f
  end
end
