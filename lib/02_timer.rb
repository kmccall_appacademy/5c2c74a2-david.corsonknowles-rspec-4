class Timer

  def initialize
    @seconds = 0
  end

  attr_reader :seconds

  def seconds=(entry = 0)
    @seconds = @seconds + entry
  end

  # format time as "00:00:00"
  def time_string

    result = ""
    hours_elapsed = @seconds / 3600
    minutes_elapsed = (@seconds - hours_elapsed * 3600) / 60
    seconds_elapsed = @seconds % 60
    "#{"0" if hours_elapsed.to_s.length == 1}#{hours_elapsed}:\
#{"0" if minutes_elapsed.to_s.length == 1}#{minutes_elapsed}:\
#{"0" if seconds_elapsed.to_s.length == 1}#{seconds_elapsed}"
#for multiline escaped strings: remove indents to get a single line result
  end

end

# def minutes
#   @minutes = @timer % 60
# end
#
# def hours
#   @hours = @minutes  % 60
# end
