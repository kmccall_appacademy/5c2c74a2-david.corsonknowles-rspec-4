class Dictionary

  def initialize
    @d = {}
  end

  def entries
    @d
  end

  def add(arg)
    if arg.class == String
      arg = {arg => nil }
    end
    @d.merge!(arg.to_h)
    # Equivalently:
    # arr = arg.to_a
    # @d[arr[0][0]] = arr[0][1]
  end

  def include?(arg)
    @d.include?(arg)
  end

  def keywords
    @d.keys.sort
  end

  def find(string)
    result = {}
    if @d.empty?
      result
    else
      @d.each do |k, v|
        result[k] = v if k.include?(string)
      end
    end
    result
  end

  def printable
    result = ""
    @d.each do |k, v|
      result = %Q{[#{k}] \"#{v}\"\n} + result
    end
    result = result.chomp
  end

end
