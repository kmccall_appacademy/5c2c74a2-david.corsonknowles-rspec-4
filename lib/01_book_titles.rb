class Book
  attr_reader :title

  def title=(entry)
    @exceptions = %w(a an the if to for that over but and though in out of \
      because except under behind beyond through)

    result = []

    entry.split.each_with_index do |word, index|
      if index == 0 || word == "i"
        result << word.capitalize
      elsif @exceptions.include?(word)
        result << word
      else
        result << word.capitalize
      end
    end

    @title = result.join(" ")
  end



end
